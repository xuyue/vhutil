create or replace view v_drgs_virtual_dept as
select
t2.ID AS UNIT_ID,
t1."ID",t1."COMP_CODE",t1."NODE_ID",t1."NODE_NAME",t1."NODE_TYPE",t1."NODE_ID_PATH",t1."SUPER_ID",t1."IS_ROOT",t1."IS_LEAF",t1."IS_LAST",t1."LEVEL_NO",t1."SCENE_CODE",t1."VERSION_ID" from
(select
cast (t.ID as varchar2(32)) as ID,
case t.NODE_TYPE when '1' then cast(d.comp_code as varchar2(200)) when '2' then cast(m.comp_code as varchar2(200))  end comp_code,
cast (t.NODE_ID as varchar2(32)) as NODE_ID,
case t.NODE_TYPE when '1' then cast(d.dept_name as varchar2(200)) when '2' then cast(m.MEMBER_NAME as varchar2(200)) else t.NODE_NAME end NODE_NAME,
t.NODE_TYPE,
t.NODE_ID_PATH,
t.SUPER_ID,
t.IS_ROOT,
t.IS_LEAF,
t.IS_LAST,
t.LEVEL_NO,
t.SCENE_CODE,
t.VERSION_ID
from SYS_DEPT_VIRTUAL_TREE t left join sys_dept d on t.NODE_ID=d.dept_id
left join SYS_VIRTUAL_MEMBER m on t.NODE_ID=m.ID where NODE_TYPE in ('1','2') and VERSION_ID='0') t1 left join up_org_unit t2 on t1.comp_code=t2.code;
comment on column V_DRGS_VIRTUAL_DEPT.UNIT_ID is '医院ID';
comment on column V_DRGS_VIRTUAL_DEPT.ID is '虚拟树ID';
comment on column V_DRGS_VIRTUAL_DEPT.COMP_CODE is '医院编号';
comment on column V_DRGS_VIRTUAL_DEPT.NODE_ID is '科室ID';
comment on column V_DRGS_VIRTUAL_DEPT.NODE_NAME is '科室名称';
comment on column V_DRGS_VIRTUAL_DEPT.NODE_TYPE is '科室类型(1-部门，2-虚拟科室)';
comment on column V_DRGS_VIRTUAL_DEPT.NODE_ID_PATH is '节点ID路径';
comment on column V_DRGS_VIRTUAL_DEPT.SUPER_ID is '上级节点';
comment on column V_DRGS_VIRTUAL_DEPT.IS_ROOT is '是否根节点(0:否，1是)';
comment on column V_DRGS_VIRTUAL_DEPT.IS_LEAF is '是否叶子节点(0:否，1是)';
comment on column V_DRGS_VIRTUAL_DEPT.IS_LAST is '是否末级(0:否，1是)';
comment on column V_DRGS_VIRTUAL_DEPT.LEVEL_NO is '级次';
comment on column V_DRGS_VIRTUAL_DEPT.SCENE_CODE is '场景编码';
comment on column V_DRGS_VIRTUAL_DEPT.VERSION_ID is '版本ID';
