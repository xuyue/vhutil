DECLARE num NUMBER;
BEGIN
  SELECT COUNT(*) into num FROM USER_TAB_COLUMNS WHERE TABLE_NAME = upper('UP_CODELIST') AND COLUMN_NAME = upper('language');
  if num<1 THEN
 execute immediate 'alter table UP_CODELIST add  language      VARCHAR2(16) default ''zh_CN''';
 execute immediate 'comment on column UP_CODELIST.language is ''国际化信息''';
  END IF;
END;
/
DECLARE num NUMBER;
BEGIN
  SELECT COUNT(*) into num FROM USER_TAB_COLUMNS WHERE TABLE_NAME = upper('UP_CODELIST') AND COLUMN_NAME = upper('created_by');
  if num<1 THEN
 execute immediate 'alter table UP_CODELIST add  created_by      VARCHAR2(64)';
 execute immediate 'comment on column UP_CODELIST.created_by is ''修改人''';
  END IF;
END;
/
