IF NOT EXISTS(SELECT * FROM sysobjects WHERE id =  object_id(N'[DRGS_PREGROUP_LOG]') AND xtype = 'U') begin
create table DRGS_PREGROUP_LOG
(
  id            VARCHAR(32) not null,
  group_time    DATE,
  aac02c        VARCHAR(32),
  opercode      VARCHAR(50),
  opername      VARCHAR(50),
  aaa30         VARCHAR(20),
  aaa01         VARCHAR(50),
  abc01c        VARCHAR(50),
  abc01n        VARCHAR(200),
  main_ope_code VARCHAR(50),
  main_ope_name VARCHAR(200),
  drg_code      VARCHAR(20),
  drg_name      VARCHAR(200),
  is_ingroup    VARCHAR(2),
  is_error      VARCHAR(2),
  error_content VARCHAR(2048),
  unit_code     VARCHAR(32)
);
execute sp_addextendedproperty 'MS_Description','预分组调用记录表','user','dbo','table','DRGS_PREGROUP_LOG',null,null;
execute sp_addextendedproperty 'MS_Description','主键ID','user','dbo','table','DRGS_PREGROUP_LOG','column','id';
execute sp_addextendedproperty 'MS_Description','时间','user','dbo','table','DRGS_PREGROUP_LOG','column','group_time';
execute sp_addextendedproperty 'MS_Description','科室','user','dbo','table','DRGS_PREGROUP_LOG','column','aac02c';
execute sp_addextendedproperty 'MS_Description','调用医师编码','user','dbo','table','DRGS_PREGROUP_LOG','column','opercode';
execute sp_addextendedproperty 'MS_Description','调用医师名称','user','dbo','table','DRGS_PREGROUP_LOG','column','opername';
execute sp_addextendedproperty 'MS_Description','住院号','user','dbo','table','DRGS_PREGROUP_LOG','column','aaa30';
execute sp_addextendedproperty 'MS_Description','姓名','user','dbo','table','DRGS_PREGROUP_LOG','column','aaa01';
execute sp_addextendedproperty 'MS_Description','主要诊断编码','user','dbo','table','DRGS_PREGROUP_LOG','column','abc01c';
execute sp_addextendedproperty 'MS_Description','主要诊断名称','user','dbo','table','DRGS_PREGROUP_LOG','column','abc01n';
execute sp_addextendedproperty 'MS_Description','主要手术操作编码','user','dbo','table','DRGS_PREGROUP_LOG','column','main_ope_code';
execute sp_addextendedproperty 'MS_Description','主要手术操作名称','user','dbo','table','DRGS_PREGROUP_LOG','column','main_ope_name';
execute sp_addextendedproperty 'MS_Description','DRG编码','user','dbo','table','DRGS_PREGROUP_LOG','column','drg_code';
execute sp_addextendedproperty 'MS_Description','DRG名称','user','dbo','table','DRGS_PREGROUP_LOG','column','drg_name';
execute sp_addextendedproperty 'MS_Description','是否入组，1:是0:否','user','dbo','table','DRGS_PREGROUP_LOG','column','is_ingroup';
execute sp_addextendedproperty 'MS_Description','是否返回错误，1:是0:否','user','dbo','table','DRGS_PREGROUP_LOG','column','is_error';
execute sp_addextendedproperty 'MS_Description','错误原因','user','dbo','table','DRGS_PREGROUP_LOG','column','error_content';
execute sp_addextendedproperty 'MS_Description','医院编码','user','dbo','table','DRGS_PREGROUP_LOG','column','unit_code';
alter table DRGS_PREGROUP_LOG add constraint PK_DRGS_PREGROUP_LOG primary key (ID);
END;
GO

