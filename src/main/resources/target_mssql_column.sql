IF NOT EXISTS(select  * from   syscolumns   where   id=object_id('UP_CODELIST')   and   name='language'  ) begin
　　alter table UP_CODELIST add language VARCHAR(16) default 'zh_CN';
    execute sp_addextendedproperty 'MS_Description','国际化信息','user','dbo','table','UP_CODELIST','column','language';
end;
IF NOT EXISTS(select  * from   syscolumns   where   id=object_id('UP_CODELIST')   and   name='created_by'  ) begin
　　alter table UP_CODELIST add created_by VARCHAR(64);
    execute sp_addextendedproperty 'MS_Description','修改人','user','dbo','table','UP_CODELIST','column','created_by';
end;
