DECLARE num NUMBER;
BEGIN
  select count(1) into num from user_tables t where t.TABLE_NAME = UPPER('DRGS_CAS_USER_MAPPING');
  if num<1 THEN
EXECUTE IMMEDIATE '
create table DRGS_CAS_USER_MAPPING
(
  id           VARCHAR2(32) not null,
  drg_account  VARCHAR2(50),
  his_emp_code VARCHAR2(50),
  his_emp_name VARCHAR2(100)
)';
EXECUTE IMMEDIATE 'comment on table DRGS_CAS_USER_MAPPING  is ''单点登录HIS与DRG系统用户对照表''';
EXECUTE IMMEDIATE 'comment on column DRGS_CAS_USER_MAPPING.id  is ''主键ID''';
EXECUTE IMMEDIATE 'comment on column DRGS_CAS_USER_MAPPING.drg_account  is ''DRG系统账号''';
EXECUTE IMMEDIATE 'comment on column DRGS_CAS_USER_MAPPING.his_emp_code  is ''HIS账户编码''';
EXECUTE IMMEDIATE 'comment on column DRGS_CAS_USER_MAPPING.his_emp_name  is ''HIS账户名称''';
EXECUTE IMMEDIATE 'alter table DRGS_CAS_USER_MAPPING  add constraint PK_DCUM primary key (ID)';
END IF;
END;
/

