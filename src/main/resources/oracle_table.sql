create table DRGS_CAS_USER_MAPPING
(
  id           VARCHAR2(32) not null,
  drg_account  VARCHAR2(50),
  his_emp_code VARCHAR2(50),
  his_emp_name VARCHAR2(100)
);
comment on table DRGS_CAS_USER_MAPPING  is '单点登录HIS与DRG系统用户对照表';
comment on column DRGS_CAS_USER_MAPPING.id  is '主键ID';
comment on column DRGS_CAS_USER_MAPPING.drg_account  is 'DRG系统账号';
comment on column DRGS_CAS_USER_MAPPING.his_emp_code  is 'HIS账户编码';
comment on column DRGS_CAS_USER_MAPPING.his_emp_name  is 'HIS账户名称';
alter table DRGS_CAS_USER_MAPPING  add constraint PK_DCUM primary key (ID);
