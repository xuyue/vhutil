/**
 * 
 */
package com.xuyue;

import java.io.File;
import java.text.ParseException;
import java.util.List;
import java.util.Scanner;

import com.viewhigh.vadp.tool.batch.sql.ConfigUtil;
import com.viewhigh.vadp.tool.batch.sql.ExcultSqlHelp;
import com.viewhigh.vadp.tool.batch.sql.GetSQLFile;
import com.viewhigh.vadp.tool.batch.sql.ParserSqlFileHelp;

/**
 * 批量执行sql
 * @author tony
 *
 */
public class BatchExecSql {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ConfigUtil.init();
	    String sqlFileType = ConfigUtil.config.getDbtype();
	    String rootFilePath = ConfigUtil.config.getSqlFileDir();
	    File rootFile = new File(rootFilePath);
	    try {
	      if (!rootFile.exists())
	        System.out.println("指定的路径错误："+ rootFilePath); 
	      if (sqlFileType.equals(ParserSqlFileHelp.FLAG_ORACLE) || sqlFileType.equals(ParserSqlFileHelp.FLAG_SQLSERVER) || sqlFileType.equals(ParserSqlFileHelp.FLAG_DM)) {
	        List<File> files = GetSQLFile.getFiles(sqlFileType, rootFile);
	        for (File file : files) {
	          System.out.println("开始解析执行文件："+ file.getName());
	          ExcultSqlHelp.excultSqlFiles(file);
	          System.out.println("执行成功："+ file.getName());
	        } 
	      } else {
	        System.out.println("数据库类型只能输入mssql或oracle");
	        System.out.println("请按回车继续.......");
	        Scanner in = new Scanner(System.in);
	        String str = in.nextLine();
	      } 
	    } catch (ParseException e) {
	      e.printStackTrace();
	      System.out.println("请按回车继续.......");
	      Scanner in = new Scanner(System.in);
	      String str = in.nextLine();
	    } 
	}

}
