package com.xuyue;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xuyue.util.FileUtil;
import com.xuyue.util.NumberUtils;
/**
 * 武汉预分组收费明细参数拆解
 * @author tony
 *
 */
public class WuHanPreGroup {
	   
    public static void printFee(String fileName) {
    	//'01':'医疗费用';'02':'护理费用';'03':'检查费用';'04':'管理费用';'05':'药品费用';'06':'耗材费用'
    	Map<String, String> dictMap =new HashMap<String, String>();
    	dictMap.put("01", "医疗费用");
    	dictMap.put("02", "护理费用");
    	dictMap.put("03", "检查费用");
    	dictMap.put("04", "管理费用");
    	dictMap.put("05", "药品费用");
    	dictMap.put("06", "耗材费用");
    	
        String path = WuHanPreGroup.class.getClassLoader().getResource(fileName).getPath();
        String s = FileUtil.readFile(path);
        JSONObject jobj = JSON.parseObject(s);
        BigDecimal yaopin=new BigDecimal(0); 
        BigDecimal haocai=new BigDecimal(0); 
        BigDecimal yiliao=new BigDecimal(0); 
        BigDecimal huli=new BigDecimal(0); 
        BigDecimal jiancha=new BigDecimal(0); 
        BigDecimal guanli=new BigDecimal(0); 
        BigDecimal total=new BigDecimal(0); 
        JSONArray movies = jobj.getJSONArray("FEES");//构建JSONArray数组
        for (int i = 0 ; i < movies.size();i++){
            JSONObject key = (JSONObject)movies.get(i);
            String CHARGEKIND = (String)key.get("CHARGEKIND");
            String CHARGEKINDNAME = (String)key.get("CHARGEKINDNAME");
            String CHARGEKINDID=((String)key.get("CHARGEKINDID"));
            String CHARGEDETAILID=((String)key.get("CHARGEDETAILID"));
            String CHARGEDETAILNAME=((String)key.get("CHARGEDETAILNAME"));
            BigDecimal AMOUNT=((BigDecimal)key.get("AMOUNT"));
            if("05".equals(CHARGEKIND)) {
            	yaopin=NumberUtils.add(yaopin, AMOUNT);
            }else if("06".equals(CHARGEKIND)) {
            	haocai=NumberUtils.add(haocai, AMOUNT);
            }else if("04".equals(CHARGEKIND)) {
            	guanli=NumberUtils.add(guanli, AMOUNT);
            }else if("03".equals(CHARGEKIND)) {
            	jiancha=NumberUtils.add(jiancha, AMOUNT);
            }else if("02".equals(CHARGEKIND)) {
            	huli=NumberUtils.add(huli, AMOUNT);
            }else if("01".equals(CHARGEKIND)) {
            	yiliao=NumberUtils.add(yiliao, AMOUNT);
            }
            System.out.println(CHARGEKIND+"~"+dictMap.get(CHARGEKIND)+"<<<"+CHARGEKINDID+"-"+CHARGEKINDNAME+"<<<"+CHARGEDETAILID+"-"+CHARGEDETAILNAME);
        }
        System.out.println("---------------------------------------------------------");
        System.out.println(dictMap.get("01")+"："+yiliao);
        total=NumberUtils.add(total, yiliao);
        System.out.println(dictMap.get("02")+"："+huli);
        total=NumberUtils.add(total, huli);
        System.out.println(dictMap.get("03")+"："+jiancha);
        total=NumberUtils.add(total, jiancha);
        System.out.println(dictMap.get("04")+"："+guanli);
        total=NumberUtils.add(total, guanli);
        System.out.println(dictMap.get("05")+"："+yaopin);
        total=NumberUtils.add(total, yaopin);
        System.out.println(dictMap.get("06")+"："+haocai);
        total=NumberUtils.add(total, haocai);
        System.out.println("---------------------------------------------------------");
        System.out.println("明细合计："+total);
    }
    
    public static void main(String[] args) {
    	printFee("1.json");
//    	System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
//    	printFee("0001536833.json");
    }
}
