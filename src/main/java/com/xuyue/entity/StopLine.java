package com.xuyue.entity;

public class StopLine {
    //车次
	private String left;
	//运行区间
	private String right;
	//备注
	private String tip;
	
	public String getLeft() {
		return left;
	}
	public void setLeft(String left) {
		this.left = left;
	}
	public String getRight() {
		return right;
	}
	public void setRight(String right) {
		this.right = right;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	

}
