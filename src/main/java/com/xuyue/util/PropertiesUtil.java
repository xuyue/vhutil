/**
 * 
 */
package com.xuyue.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author tony
 *
 */
public class PropertiesUtil {

	public static String getValue(String key) {
		String value="";
		Properties properties = new Properties();
		InputStream inputStream = Object.class.getResourceAsStream("/application.properties");
		try {
			 properties.load(inputStream);
			 value=properties.getProperty(key);
		} catch (IOException e) {
			throw new RuntimeException("解析配置文件错误：" + e.getMessage());
		} 
		return value;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(getValue("pyExePath"));
	}

}
