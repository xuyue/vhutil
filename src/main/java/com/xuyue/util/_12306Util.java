/**
 * 
 */
package com.xuyue.util;

import java.util.List;

import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;

/**
 * @author tony
 *
 */
public class _12306Util {
	public static CookieStore cookieStore = new BasicCookieStore();
	static CloseableHttpClient httpClient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
	
	public static String initUrl = "https://kyfw.12306.cn/otn/login/init";
	public static String railUrl = "https://kyfw.12306.cn/otn/HttpZF/logdevice"+
			//"?algID=8iIPwpB28l"+
			"?algID=XWptt1kXbs"+
			//"&hashCode=XQTW18RSf_dOYnqpGaj_yA-WKSsKbH77QE7zN-cCdB0"+
			"&hashCode=K7H-SogpeC4-tIYLqCXJabQWEnBXNROVnaPuQXU8Vec"+
			"&FMQw=0"+
			"&q4f3=zh-CN"+
			"&VPIf=1"+
			"&custID=133"+
			"&VEek=unknown"+
			"&dzuS=0"+
			"&yD16=0"+
			"&EOQP=89f60554e6cb588cf7dcc391a91488a1"+
			"&lEnu=176525634"+
			"&jp76=52d67b2a5aa5e031084733d5006cc664"+
			"&hAqN=Win32"+
			"&platform=WEB"+
			"&ks0Q=d22ca0b81584fbea62237b14bd04c866"+
			"&TeRS=1010x1680"+
			"&tOHY=24xx1050x1680"+
			"&Fvje=i1l1o1s1"+
			"&q5aJ=-8"+
			"&wNLf=99115dfb07133750ba677d055874de87"+
			"&0aew=Mozilla/5.0%20(Windows%20NT%2010.0;%20Win64;%20x64)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/76.0.3809.100%20Safari/537.36"+
			"&E3gR=a54e6d6499e595ae2cfaf80eda685cc2"+
			"&timestamp="+String.valueOf(System.currentTimeMillis());
	
	public static String accept = "*/*";
	public static String acceptEncoding = "gzip, deflate, br";
	public static String acceptLanguage = "zh-CN,zh;q=0.9";
	public static String userAgent ="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36";
	
	public static String shutdownUrlPrifix="https://mobile.12306.cn/otsmobile/h5/otsbussiness/notice";
	
	public static CloseableHttpClient getHttpClient() {
		return httpClient;
	}
	
	public static CookieStore getCookieStore() {
		return cookieStore;
	}
	
	public static void printCookies() {
		System.out.println("打印cookie信息：");
		List<org.apache.http.cookie.Cookie> cs = getCookieStore().getCookies();
		for (int i = 0; i < cs.size(); i++) {
			System.out.println(cs.get(i).getName()+":"+cs.get(i).getValue());
		}
	}
	
	public static void addRailCookies(String exp, String dfp) {
		BasicClientCookie expCookie = new BasicClientCookie("RAIL_EXPIRATION", exp);
        expCookie.setDomain("kyfw.12306.cn");
        expCookie.setPath("/");
        cookieStore.addCookie(expCookie);
        BasicClientCookie dfpCookie = new BasicClientCookie("RAIL_DEVICEID", dfp);
        dfpCookie.setDomain("kyfw.12306.cn");
        dfpCookie.setPath("/");
        cookieStore.addCookie(dfpCookie);
	}
}
